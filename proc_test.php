<?php

$timeout=10; //раз во сколько секунд опрашивать потоки(при 0 мы будем опрашивать
//потоки постоянно, но этот режим потребляет много памяти
$streams=array();//массив потоков
$handles=array();//массив ссылок на потоки
$all_pipes=array();//массив настроек для каждого потока
$maxthreads=4;//количество потоков


for ($id=0; $id <= $maxthreads; $id++) {
$error_log="/home/remamba-dev/public_html/vk_script/proc" . $id . ".txt";//файл ошибок соответствует номеру потока
$descriptorspec=array(
    0 => array("pipe", "r"),
    1 => array("pipe", "w"),
    2 => array("file", $error_log, "w")
);
$cmd='/usr/bin/php /home/remamba-dev/public_html/vk_script/curl_vk_auth.php';//команда которую вызывает поток,
//в данном случае мы вызываем исполнение скрипта потока
$handles[$id]=proc_open($cmd, $descriptorspec, $pipes);//создаем поток
$streams[$id]=$pipes[1];
$all_pipes[$id]=$pipes;
}

while (count($streams)) {//пока все потоки не исполнились
    $read=$streams;
    stream_select($read, $w=null, $e=null, $timeout);//опрашиваем потоки
    foreach ($read as $r) {
        $id=array_search($r, $streams);
        echo stream_get_contents($all_pipes[$id][1]);//выводим на экран все что возращает скрипт
        if (feof($r)) {//если поток выполнился возвращается EOF
            //закрываем поток
            fclose($all_pipes[$id][0]);
            fclose($all_pipes[$id][1]);
            $return_value=proc_close($handles[$id]);
            unset($streams[$id]);//удаляем информацию о нем из массива
        }
    }
}
