function explode( delimiter, string ) {
    var emptyArray = { 0: '' };
    if ( arguments.length != 2
        || typeof arguments[0] == 'undefined'
        || typeof arguments[1] == 'undefined' )
    {
        return null;
    }
    if ( delimiter === ''
        || delimiter === false
        || delimiter === null )
    {
        return false;
    }
    if ( typeof delimiter == 'function'
        || typeof delimiter == 'object'
        || typeof string == 'function'
        || typeof string == 'object' )
    {
        return emptyArray;
    }
    if ( delimiter === true ) {
        delimiter = '1';
    }
    return string.toString().split ( delimiter.toString() );
}

function getRandomFromArray(arr, isKey) {
    if(isKey) {
        var key = Math.floor(Math.random() * arr.length);
        return {key: key, value: arr[key]};
    } else {
        return arr[Math.floor(Math.random() * arr.length)];
    }
}

function extractFromArray(arr, key) {
    var ret = [];
    for(var k in arr) {
        if(key != k) {
            ret.push(arr[k]);
        }
    }
    return ret;
}

function getCounters(userInfo, obj) {
    var params = {action: 'getFriends', name: userInfo.login, password: userInfo.password};
    $.ajax({
        context: obj,
        data: params,
        dataType: 'json',
        error: function(err){
            $(this).find('td:eq(0) div').css('background-color', 'red');
            $(this).find('td:eq(3)').text('NO');
            $(this).find('td:eq(4)').text('NO');
            $(this).find('td:eq(5)').text('NO');
            console.log(err.responseText);
        },
        success: function(data) {
            $(this).find('td:eq(0) div').css('background-color', 'green');
            $(this).find('td:eq(3)').text(data.countFriends);
            $(this).find('td:eq(4)').text(data.countRequests);
            $(this).find('td:eq(5)').text(data.countAllRequests);
            setTimeout(function(){ getCounters(userInfo, obj); }, 120000);
            console.log(data);
        }
    });
}

function postToTopic() {
    this.postMessageInterval = null;
    this.randUser = 0;
    this.lastNumber = 0;
    this.lastPostId = '';
    this.lastHash = '';
    this.url = '';
    this.topicId = 0;
    this.init = function(key) {
        var t = this;
        t.topicId = key;
        t.randUser = getRandomFromArray(usersWithNames);
        t.lastNumber = 0;
        t.lastPostId = 0;
        t.lastHash = '';
        t.url = topics[key]['topic_url'];
        t.getLastMessage();
    };
    this.findUsers = function(id){
        for(var k in usersWithNames) {
            var v = usersWithNames[k];
            if(v.uid == id) {
                return k;
            }
        }
        return false;
    };
    this.deletePost = function(data) {// удаляем сообщение
        var t = this;
        var params = data;
        params['action'] = 'topicDeleteComment';
        params['name'] = t.randUser.login;
        $.ajax({
            data: params,
            dataType: 'json',
            error: function(err){
                console.log(err.responseText);
            },
            success: function(data) {
                var tr = $('tr#' + t.randUser.blockId);
                console.log(data);
            }
        });
    };
    this.checkCountMessages = function(number) {// Проверяем количество последних сообщений и делаем перепост
        var t = this;
        $.ajax({
            data: {action: 'checkLastMessages', url: t.url, name: t.randUser.login, users: usersWithNames, number: number},
            dataType: 'json',
            error: function(err){
                console.log(err.responseText);
                clearInterval(t.postMessageInterval);
            },
            success: function(data) {
                var tr = $('tr#' + t.randUser.blockId);
                if(data.count >= parseInt(settings[2]['setting_value'], 10) || data.notFound) {
                    if(parseInt(settings[3]['setting_value'], 10) > 0 && t.lastPostId && t.lastHash) {
                        t.deletePost({commentId: t.lastPostId, hash: t.lastHash});
                    }
                    t.randUser = getRandomFromArray(usersWithNames);
                    console.log('ret', data, 'user', t.randUser);
                    t.getLastMessage();
                }
            }
        });
    };
    this.postingQueque = function(iteration, users) {
        var t = this;
        var user = getRandomFromArray(users, true);
        users = extractFromArray(users, user.key);
        t.getLastMessage(user.value, true);
        if(iteration > 1) {
            t.postingQueque(iteration - 1, users);
        }
    };
    this.getLastMessage = function(postUser, holdQueque) {// получаем последнюю запись
        var t = this;
        if(!postUser) {
            postUser = t.randUser;
        }
        if(!holdQueque) {
            holdQueque = false;
        }
        $.ajax({
            data: {action: 'topicLastRecords', url: t.url, name: postUser.login},
            dataType: 'json',
            error: function(err){
                var tr = $('tr#' + postUser.blockId);
                tr.find('td:eq(0) div').css('background-color', 'red');
                tr.find('td:eq(6)').text('PHP ERROR');
                console.log(err.responseText);
            },
            success: function(data) {
                var tr = $('tr#' + postUser.blockId);
                if(data.lastRecords) {
                    t.postMessage(data.lastRecords, postUser, holdQueque);
                    tr.find('td:eq(0) div').css('background-color', 'green');
                    tr.find('td:eq(6)').text(data.lastRecords);
                } else {
                    tr.find('td:eq(0) div').css('background-color', 'red');
                    tr.find('td:eq(6)').text('PAGE NOT FOUND');
                    console.log('BAD PAGE:', data.error);
                }
            }
        });
    };
    this.postMessage = function(number, postUser, holdQueque) {// постим сообщение
        var t = this;
        if(!postUser) {
            postUser = t.randUser;
        }
        var params = {
            action: 'topicPostComment',
            url: t.url,
            number: number,
            user: postUser,
            comment: getRandomFromArray(phrases)
        };
        if(!number || !postUser) return false;
        $.ajax({
            data: params,
            dataType: 'json',
            error: function(err){
                var tr = $('tr#' + postUser.blockId);
                tr.find('td:eq(0) div').css('background-color', 'red');
                tr.find('td:eq(7)').text('PHP ERROR');
                console.log(err.responseText);
            },
            success: function(data) {
                var tr = $('tr#' + postUser.blockId);
                if(data.commentId) {
                    clearInterval(t.postMessageInterval);
                    t.iterator(data.number);
                    var countPosts = parseInt(settings[4]['setting_value'], 10);
                    if(countPosts > 1 && !holdQueque) {
                        var thisUserKey = t.findUsers(postUser.uid);
                        var tmpUsers = [];
                        if(thisUserKey !== false) {
                            tmpUsers = extractFromArray(usersWithNames, thisUserKey);
                        }
                        t.postingQueque(countPosts - 1, tmpUsers);
                    }
                    console.log('iterator init!');
                    tr.find('td:eq(0) div').css('background-color', 'green');
                    tr.find('td:eq(7)').text(data.commentId);
                    var logs = tr.find('td:eq(8) div');
                    logs.html(logs.html() + 'ID :' + t.topicId + ' Comment added :' + data.txt + ' at ' + (new Date()).toLocaleTimeString() + '<br />');
                    t.lastNumber = data.number;
                    t.lastPostId = data.commentId;
                    t.lastHash = data.hash;
                } else {
                    tr.find('td:eq(0) div').css('background-color', 'red');
                    tr.find('td:eq(7)').text('COMMENT NOT ADDED: ' + t.url + '?offset=' + number);
                }
            }
        });
    };
    this.iterator = function(number) {// бесконечные итерации
        var t = this;
        t.postMessageInterval = setInterval(function(){
            t.checkCountMessages(number);
        }, parseInt(settings[1]['setting_value'], 10) * 60 * 1000);
    };
}

$(function(){
    $.ajaxSetup({
        type: 'GET',
        url: 'curl_vk_actions.php',
        timeout: 180000
    });
    $('#startScripts').click(function(){
		$.ajax({ async: false, data: {clearCache: true} });
        for(var k in users) {
            var user = users[k];
            var params = {action: 'login', name: user['user_login'], password: user['user_password']};
            $.ajax({
                context: $('.main tr#user' + k),
                data: params,
                dataType: 'json',
                error: function(err, errType){
                    $(this).find('td:eq(0) div').css('background-color', 'red');
                    $(this).find('td:eq(2)').text('NO');
                    console.log(err, errType);
                },
                success: function(data) {
                    var tr = $(this);
//                    console.log(tr, '-->', data);
                    if(data.userUid) {
                        $('#postingToTopics').show();
                        tr.find('td:eq(0) div').css('background-color', 'green');
                        tr.find('td:eq(2)').html(data.rusName + '<br />' + data.userUid + ' - ' + data.userName);
                        usersWithNames.push({login: data.login, pass: data.password, uid: data.userUid, name: data.userName, blockId: tr.attr('id')});
                        setTimeout(function(){
                            getCounters(data, tr);
                        }, 3000);
                    } else if(data.blocked) {
                        tr.find('td:eq(0) div').css('background-color', 'red');
                        tr.find('td:eq(2)').html('<font color="red">Юзер Блокирован</font><br />' + data.blocked);
                    } else if(data.notAuth) {
                        tr.find('td:eq(0) div').css('background-color', 'red');
                        tr.find('td:eq(2)').html('<font color="red">Ошибка Авторизации</font>');
					} else if(data.parseError) {
                        tr.find('td:eq(0) div').css('background-color', 'red');
                        tr.find('td:eq(2)').html('<font color="red">Неизвестная Ошибка</font>');
						console.log(tr);
						$.ajax({
							context: tr,
							data: {action: 'login', name: data.login, password: data.password, noProxy: 1},
							dataType: 'json',
							error: function(err, errType){ console.log(err, errType); },
							success: function(data) {
								var tr = $(this);
								console.log(tr);
								if(data.userUid) {
									$('#postingToTopics').show();
									tr.find('td:eq(0) div').css('background-color', 'green');
									tr.find('td:eq(2)').html(data.rusName + '<br />' + data.userUid + ' - ' + data.userName);
									usersWithNames.push({login: data.login, pass: data.password, uid: data.userUid, name: data.userName, blockId: tr.attr('id')});
									setTimeout(function(){
										getCounters(data, tr);
									}, 3000);
								} else if(data.blocked) {
									tr.find('td:eq(0) div').css('background-color', 'red');
									tr.find('td:eq(2)').html('<font color="red">Юзер Блокирован</font><br />' + data.blocked);
								} else if(data.notAuth) {
									tr.find('td:eq(0) div').css('background-color', 'red');
									tr.find('td:eq(2)').html('<font color="red">Ошибка Авторизации</font>');
								}
							}
						});
                    }
                }
            });
        }
    });
    $('#postingToTopics').click(function(){
        for(var k in topics) {
            setTimeout(function(key){
                var post = new postToTopic();
                post.init(key);
            }, 2000 * k, [k]);
        }
    });
});
