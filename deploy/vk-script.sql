-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Окт 23 2012 г., 20:42
-- Версия сервера: 5.5.24
-- Версия PHP: 5.3.10-1ubuntu3.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `vk-script`
--

-- --------------------------------------------------------

--
-- Структура таблицы `phrases`
--

CREATE TABLE IF NOT EXISTS `phrases` (
  `phrase_id` int(11) NOT NULL AUTO_INCREMENT,
  `phrase_txt` varchar(255) NOT NULL,
  PRIMARY KEY (`phrase_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `phrases`
--

INSERT INTO `phrases` (`phrase_id`, `phrase_txt`) VALUES
(1, 'добавь');

-- --------------------------------------------------------

--
-- Структура таблицы `proxies`
--

CREATE TABLE IF NOT EXISTS `proxies` (
  `proxy_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `proxy_ip` varchar(20) NOT NULL,
  `proxy_port` mediumint(6) unsigned NOT NULL,
  PRIMARY KEY (`proxy_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `proxies`
--

INSERT INTO `proxies` (`proxy_id`, `proxy_ip`, `proxy_port`) VALUES
(1, '80.58.250.68', 80);

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `setting_name` varchar(255) NOT NULL,
  `setting_value` int(11) NOT NULL,
  `setting_check` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `settings`
--

INSERT INTO `settings` (`setting_id`, `setting_name`, `setting_value`, `setting_check`) VALUES
(1, 'Частота проверки новых сообщений', 1, 0),
(2, 'Через сколько сообщений постим свои', 5, 0),
(3, 'Удалять ли старые сообщения', 0, 1),
(4, 'Сколько сообщений постим за раз', 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `topics`
--

CREATE TABLE IF NOT EXISTS `topics` (
  `topic_id` int(11) NOT NULL AUTO_INCREMENT,
  `topic_url` varchar(255) NOT NULL,
  `topic_title` varchar(255) NOT NULL,
  PRIMARY KEY (`topic_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `topics`
--

INSERT INTO `topics` (`topic_id`, `topic_url`, `topic_title`) VALUES
(1, 'http://vk.com/topic-18867871_26036582', 'ДОБАВЬ В ДРУЗЬЯ');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(50) NOT NULL,
  `user_password` varchar(50) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
