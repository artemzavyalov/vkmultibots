<?php

require_once 'inc.php';

// имитируем браузер
$user_agent = 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.79 Safari/537.4';
// что именно делаем
$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : false;
// номер телефона
$login = isset($_REQUEST['name']) ? $_REQUEST['name'] : false;
// пароль
$password = isset($_REQUEST['password']) ? $_REQUEST['password'] : false;
// URL
$url = isset($_REQUEST['url']) ? $_REQUEST['url'] : false;
// запрос без прокси-сервера
$noProxy = isset($_REQUEST['noProxy']) ? true : false;
// прокси
$ipProxy = $noProxy ? false : getProxy();

// файлы с куками
$cookie = 'cookies/' . $login . '.txt';
// последние 4 цифры телефонного номера
$lastFourNumbers = substr($login, -4);

$headers = '';
$content = '';

switch($action) {
	case 'clearCache':
		clearDir('cookies');
		clearDir('authLogs');
		break;
    case 'login':// Процедура логинизации
        if(is_file(ROOT_DIR . '/' . $cookie)) {
            unlink(ROOT_DIR . '/' . $cookie);
        }
        $result = send('http://login.vk.com/','act=login&success_url=&fail_url=&try_to_login=1&to=&vk=&email='.$login.'&pass='.$password,'http://m.vk.com/login?fast=1&hash=&s=0&to=');
		file_put_contents('authLogs/' . $login . '.txt', serialize($result));
        $headers = parseHeaders($result[0]);
        $content = $result[1];
		$parseContent = str_get_html($content);
        if($message = isBlocked($parseContent)) {
            echo json_encode(array('login' => false, 'userUid' => 0, 'blocked' => $message));
        } else {
            securityCheck();
            $match = array();
            $content = str_replace("\n", '', $content);
            preg_match('/var\s+vk\s*=\s*\{.{5,50}\s+id\s*:\s*(\d*)\s*.*\}.*/i', $content, $match);
            if(isset($match[1]) && $match[1] != '0') {
                $uid = intval($match[1]);
                $name = substr($parseContent->find('td#myprofile_wrap a', 0)->href, 1);

                $match = array();
                preg_match('/.*feed\.init\(\{.*<a\s+class=\\\"author\\\"\s*href=\\\".{10,25}\\\">(.{1,40})<.{2}a>.*\}\).*/i', $content, $match);
                if(isset($match[1])) $nameRus = iconv("WINDOWS-1251", "UTF-8//IGNORE", $match[1]);
                else $nameRus = 'Не определено';
                echo json_encode(array('login' => true, 'userUid' => $uid, 'userName' => $name, 'rusName' => $nameRus, 'login' => $login, 'password' => $password));
            } else {
				$message = $parseContent->find('#message b', 0);
				if($message) {
					echo json_encode(array('login' => false, 'userUid' => 0, 'notAuth' => true));
				} else {
					if($message = isBlocked($parseContent)) {
						echo json_encode(array('login' => false, 'userUid' => 0, 'blocked' => $message));
					} else {
						echo json_encode(array('login' => false, 'userUid' => 0, 'login' => $login, 'password' => $password, 'parseError' => true));
					}
				}
            }
        }

        break;
    case 'getFriends':// Парсинг количества друзей
        $answer = array();
        $result = send('http://vk.com/friends?section=all', '', 'http://vk.com/');
        $content = $result[1];
        $parseContent = str_get_html($content);
        $answer['countFriends'] = getNumbers($parseContent->find('div#friends_summary', 0)->plaintext);

        $result = send('http://vk.com/friends?section=requests', '', 'http://vk.com/friends?section=all');
        $content = $result[1];
        $parseContent = str_get_html($content);
        $answer['countRequests'] = getNumbers($parseContent->find('li#tab_requests .tab_word .count', 0)->plaintext);

        $result = send('http://vk.com/friends?section=all_requests', '', 'http://vk.com/friends?section=requests');
        $content = $result[1];
        $parseContent = str_get_html($content);
        $answer['countAllRequests'] = getNumbers($parseContent->find('div#friends_summary', 0)->plaintext);

        echo json_encode($answer);

        break;
    case 'topicLastRecords':// Парсинг страницы с последним сообщением
        if(!$url ) {
            echo json_encode(array('error' => false));
            die;
        }
        $lastRecords = checkTopic($url);
        if($lastRecords) {
            echo json_encode(array('lastRecords' => intval($lastRecords)));
        } else {
            echo json_encode(array('error' => $url));
        }

        break;
    case 'topicPostComment':// Постинг сообщения и возврат его id
        $number =   isset($_REQUEST['number'])   ? $_REQUEST['number']   : false;
        $user =     isset($_REQUEST['user'])     ? $_REQUEST['user']     : false;
        $comment =  isset($_REQUEST['comment'])  ? $_REQUEST['comment']  : false;
        if(!isset($user['login']) || !$number ) {
            echo json_encode(array('error' => true));
            die;
        }
        $cookie =   'cookies/' . $user['login'] . '.txt';

        $result = send($url, '', 'http://vk.com');
        $content = $result[1];
        $match = array();
        $content = str_replace("\n", '', $content);
        preg_match('/board\.inittopic\s*\(\s*\{.*topic\:\s*\\\'(.*)\\\'\,\s*owner.*hash\:\s*\\\'(.*)\\\'\,\s*url.*\}\s*\,/i', $content, $match);
        $topic = $match[1];
        $hash = $match[2];

        $parseContent = str_get_html($content);
        $lastId = explode('_', $parseContent->find('div.bp_post', -1)->id);
        $lastId = $lastId[1];

        $post = array(
            'act' => 'post_comment',
            'al' => '1',
            'topic' => $topic,
            'hash' => $hash,
            'comment' => $comment,
            'last' => $lastId,
        );

        $result = send('http://vk.com/al_board.php', http_build_query($post), $url);

        $comm = findLastComments($url, $number, array($user));

        if(count($comm) == 0) {
            echo json_encode(array('error' => true));
        } else {
            $last = $comm[count($comm) - 1];
            echo json_encode(array('commentId' => $last['id'], 'number' => $last['number'], 'hash' => $hash, 'txt' => $comment));
        }

        break;
    case 'topicDeleteComment':// Удаление сообщения
        $commentId =  isset($_REQUEST['commentId'])  ? $_REQUEST['commentId']  : false;
        $hash =       isset($_REQUEST['hash'])       ? $_REQUEST['hash']       : false;

        $post = array(
            'act' => 'delete_comment',
            'al' => '1',
            'post' => $commentId,
            'hash' => $hash,
        );

        $result = send('http://vk.com/al_board.php', http_build_query($post), 'vk.com');

        break;
    case 'checkLastMessages':// Проверяем количество последних наших сообщений(не больше чем 40)
        $number =     isset($_REQUEST['number'])    ? $_REQUEST['number']    : false;
        $users =      isset($_REQUEST['users'])     ? $_REQUEST['users']     : false;

        $comm = findLastComments($url, $number, $users);

        if(count($comm) > 0) {
            $last = $comm[count($comm) - 1];
            $count = getCountMessages($url, $last['number'], $last['id']);
            echo json_encode(array('count' => $count));
        } else {
            echo json_encode(array('notFound' => 1));
        }

        break;
}
exit;

