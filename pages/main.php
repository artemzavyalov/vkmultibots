<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
    <title>MAIN</title>
    <style type="text/css">
        .indicator {
            width: 10px;
            height: 10px;
            border: 1px solid black;
            border-radius: 6px;
        }
        .logs {
            width: 400px;
            height: 50px;
            overflow: auto;
        }
    </style>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script src="js/actions.js"></script>
    <script>
        var users = <?=json_encode($users)?>;
        var settings = <?=json_encode($settings)?>;
        var topics = <?=json_encode($topics)?>;
        var phrases = <?=json_encode($phrases)?>;
        var usersWithNames = [];
        var postMessageInterval = null;
        var randUser = [];
        var lastNumber = [];
        var lastPostId = [];
        var lastHash = [];
        var url = [];
    </script>
</head>
<body>
<a href="settings.php">settings</a>
<h2>Vk multi users</h2>

<button id="startScripts">login</button>
<button id="postingToTopics" style="display: none;">posting</button>

<table class="main" cellpadding="5" cellspacing="0" border="1">
    <thead>
    <tr align="center"><td></td><td colspan="5">Пользователи</td><td colspan="3">Постинг</td></tr>
    <tr align="center"><td></td>
        <td>Пользователь</td><td>Информация</td><td>друзья</td><td>запросы</td><td>подписки</td>
        <td>Страница</td><td>ID сообщения</td><td>Логи</td></tr>
    </thead>
    <tbody>
    <?php
    foreach($users as $k => $user):
        ?>
    <tr id="user<?=$k?>"><td><div class="indicator"></div></td>
        <td><?=$user['user_login'].':'.$user['user_password']?></td><td></td><td></td><td></td><td></td>
        <td></td><td></td><td><div class="logs"></div></td></tr>
        <?php
    endforeach;
    ?>
    </tbody>
</table>
</body>
