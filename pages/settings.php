<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
    <title>SETTINGS</title>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
</head>
<body>
<a href="index.php">назад</a>
<h2>Settings</h2>
<script>
    function saveForm(names) {
        var params = {};
        for(var k in names) {
            var val = names[k];
            params[val] = $('form [name='+val+']').val();
        }
        $('body').append('<div id="waiting" style="position: absolute; top: 0px; left: 0px; width: 100%; height: '+($('body').height() + 23)+'px; background-color: rgba(201, 201, 201, 0.5);"></div>');
        $.ajax({
            type: 'POST',
            url: 'settings.php',
            data: params,
			timeout: 5000,
			complete: function() {
				$('#waiting').remove();
			},
            error: function(err){ console.log(err); },
            success: function(data){ console.log(data); }
        });
    }
    function saveTopic(topicId) {
        var txt = $('.badTopic[data-topic='+topicId+']').val();
        if(txt) {
            $.ajax({
                type: 'POST',
                url: 'settings.php',
                data: {saveTopic: topicId, saveTxt: txt},
                error: function(err){ console.log(err); },
                success: function(data){
                    console.log(data);
                    $('#topicTr'+topicId).remove();
                    if($('.badTopics').length == 0) {
                        $('.badTopics').parent().remove();
                    }
                }
            });
        }
        return false;
    }
</script>
<form action="settings.php" method="POST" accept-charset="UTF-8">
    <?php
    $names = array();
    foreach($settings as $key => $setting):
        $names[] = 'setting_' . $key;
        if($setting['setting_check'] == '0'):?>
            <div>
                <span><?=$setting['setting_name']?>: </span>
                <input type="text" name="setting_<?=$key?>" value="<?=$setting['setting_value']?>" />
            </div>
        <?php else: ?>
            <div>
                <span><?=$setting['setting_name']?>: </span>
                <input type="hidden" name="setting_<?=$key?>" value="<?=($setting['setting_value'] > 0 ? '1' : '0')?>" />
                <input type="checkbox" data-key="<?=$key?>" <?=($setting['setting_value'] > 0 ? 'checked="checked"' : '')?> />
            </div>
        <?php endif; ?>
    <?php
    endforeach;
    ?>
    <button onclick="saveForm(['<?=implode('\',\'', $names)?>']); return false;">Сохранить</button>
    <br />
    <div style="margin-top: 20px;"><p >Список пользователей:</p><textarea name="users" style="width: 300px; height: 100px;"><?=$usersStr?></textarea>
        <button onclick="saveForm(['users']); return false;">Сохранить</button>
    </div>
    <div style="margin-top: 20px;"><p >Список проксей:</p><textarea name="proxies" style="width: 300px; height: 100px;"><?=$proxiesStr?></textarea>
        <button onclick="saveForm(['proxies']); return false;">Сохранить</button>
    </div>
    <div style="margin-top: 20px;"><p >Список топиков:</p>
        <div style="margin-top: 20px; margin-left: 500px;">
            <table class="badTopics">
            <? foreach($badTopics as $topic): ?>
                <tr id="topicTr<?=$topic['badTopic_id']?>">
                    <td><a target="_about" href="<?=$topic['badTopic_url']?>"><?=$topic['badTopic_url']?></a>&nbsp;:</td>
                    <td><input size="50" type="text" class="badTopic" data-topic="<?=$topic['badTopic_id']?>" /></td>
                    <td><button onclick=" return saveTopic('<?=$topic['badTopic_id']?>')">save</button></td>
                </tr>
            <? endforeach; ?>
            </table>
        </div>
        <textarea name="topics" style="width: 350px; height: 100px;"><?=$topicsStr?></textarea>
        <button onclick="saveForm(['topics']); return false;">Сохранить</button>
    </div>
    <div style="margin-top: 20px;"><p >Список фраз:</p><textarea name="phrases" style="width: 350px; height: 100px;"><?=$phrasesStr?></textarea>
        <button onclick="saveForm(['phrases']); return false;">Сохранить</button>
    </div>
    <button type="submit">Сохранить всё сразу</button>
</form>
<script>
    $('input[type=checkbox]').click(function(){
        var key = $(this).attr('data-key');
        var checked = $(this).attr('checked');
        if(checked) {
            $('input[name=setting_'+key+']').val('1');
        } else {
            $('input[name=setting_'+key+']').val('0');
        }
    });
</script>
</body>
