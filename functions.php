<?php

function send($url, $post, $refer) {
    global $user_agent, $cookie, $ipProxy;
//    echo time() . ' : ' . $ipProxy . ' - ' . $url . '<br />';
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
//    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
//    curl_setopt($ch, CURLOPT_TIMEOUT, 5);
//    curl_setopt($ch, CURLOPT_ENCODING, '');
//    curl_setopt($ch, CURLOPT_LOW_SPEED_LIMIT, 1);
//    curl_setopt($ch, CURLOPT_LOW_SPEED_TIME, 5);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    curl_setopt($ch, CURLOPT_REFERER, $refer);
    curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
    curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
//    curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	if($ipProxy) {
		//curl_setopt($ch, CURLOPT_PROXY, $ipProxy);
	}
    $result = curl_exec($ch);
    $delimiter = '<!DOCTYPE';
    $answer = explode($delimiter, $result);
    if(!isset($answer[1])) {
        $answer = $result;
    } else {
        $answer[1] = $delimiter . $answer[1];
    }
    /*if(!$answer) {
        $ipProxy = getProxy();
        if($ipProxy == '0.0.0.0') {
            echo 'BAD PROXIES! ' . curl_error($ch);
        } else {
            return send($url, $post, $refer);
        }
    }*/
    return $answer;
}

function isNotContent($content) {
    $content = preg_replace('/\s*/i', '', $content);
    return !strlen($content);
}

// Парсим строку с заголовками в массив
function parseHeaders($headers) {
    $ret = array();
    $delimiter = 'HTTP/1';
    $blocks = explode($delimiter, $headers);
    if(count($blocks[0]) < 2) unset($blocks[0]);

    foreach($blocks as $k => $v) {
        $v = $delimiter . $v;
        $blocks[$k] = explode("\n", $v);
    }
    foreach($blocks as $k => $v) {
        foreach($v as $vals) {
            $match = array();
            preg_match('/(.*):\s(.*)/', $vals, $match);
            if(empty($match)) {
                $ret[$k][$vals] = '';
            } else {
                $ret[$k][$match[1]] = $match[2];
            }
        }
    }
    return $ret;
}

function isBlocked($parseContent) {
    global $headers, $content;
	$message = $parseContent->find('#login_blocked_wrap .login_blocked_centered b', 0);
    if($message) {
        return iconv("WINDOWS-1251", "UTF-8//IGNORE", $message->plaintext);
    } else {
        return false;
    }
}

// Если контакт просит подтверждение последними четырьмя цифрами телефона, то подтверждаем
function securityCheck() {
    global $headers, $content, $lastFourNumbers;
    foreach($headers as $head) {
        $content = str_replace("\n", '', $content);
        $match = array();
        preg_match('/cur\.submitsecuritycheck\s*=\s*function\s*\(\s*\)\s*\{(.*)};\s*/i', $content, $match);
        if(count($match)) {
            $match = array();
            preg_match('/cur\.submitsecuritycheck\s*=\s*function\s*\(\s*\)\s*\{.*var\s*params\s*=\s*\{(.*)\};.*return false;\s*\};\s*/i', $content, $match);
            $parseArray = explode(', ', $match[1]);
            $query = array();
            foreach($parseArray as $elem) {
                $elem = explode(': ', $elem);
                if($elem[0] == 'code') {
                    $query[$elem[0]] = $lastFourNumbers;
                } else {
                    $query[$elem[0]] = str_replace("'", '', $elem[1]);
                }
            }
            $result = send('http://vk.com/login.php', http_build_query($query), 'http://login.vk.com/');
            $headers = parseHeaders($result[0]);
            $content = $result[1];
            return true;
        }
    }
    return false;
}

// Проверка топика на валидность
function checkTopic($url, $getTitle = false) {
    $result = send($url, '', 'http://vk.com/');
    $content = $result[1];
    if($getTitle) {
        $parseContent = str_get_html($content);
        $thisDiv = $parseContent->find('div#bt_pages', 0);
        if($thisDiv) {
            $content = str_replace("\n", '', $content);
            preg_match('/<title>(.*)<\/title>/i', $content, $match);
            if(isset($match[1])) {
                $topicName = explode('|', $match[1]);
                $ret = trim($topicName[1]);
            } else {
                $ret = false;
            }
        } else {
            $ret = false;
        }
    } else {
        $parseContent = str_get_html($content);
        $thisDiv = $parseContent->find('div#bt_pages', 0);
        if($thisDiv) {
            $lastHref = $thisDiv->last_child()->href;
            $lastRecords = substr($lastHref, strpos($lastHref, '?offset=') + 8);
            $ret = $lastRecords;
        } else {
            $ret = false;
        }
    }
    return $ret;
}

// Найти все последнии комментарии пользователей $users начиная с $number
function findLastComments($url, $number, $users) {
    $result = send($url . '?offset=' . $number, '', 'http://vk.com');
    $content = $result[1];
    $parseContent = str_get_html($content);
    $count = 0;
    $finded = array();
    foreach($parseContent->find('div.bp_post') as $div) {
        $author = substr($div->find('a.bp_author', 0)->href, 1);
        foreach($users as $user) {
            if($author == $user['name'] || $author == 'id' . $user['uid']) {
                $finded[] = array('id' => substr($div->id, 4), 'number' => $number);
                break;
            }
        }
        $count++;
    }
    if($count >= 20) {
        $number += 20;
        $finded = array_merge($finded, findLastComments($url, $number, $users));
    }
    return $finded;
}

// Количество сообщений после указанного
function getCountMessages($url, $number, $id) {
    $result = send($url . '?offset=' . $number, '', 'http://vk.com/');
    $content = $result[1];
    $parseContent = str_get_html($content);
    $count = null;
    $divs = $parseContent->find('.bp_post');
    if($divs) {
        foreach($divs as $div){
            if(is_numeric($count)) {
                $count++;
            }
            if($div->id == 'post' . $id) {
                $count = 0;
            }
        }
        $result = send($url . '?offset=' . ($number + 20), '', 'http://vk.com/');
        $parseContent = str_get_html($result[1]);
        $divs = $parseContent->find('.bp_post');
        if($divs) {
            $count += 20;
        }
    }
    return (int)$count;
}

function getNumbers($str) {
    $matches = array();
    $ret = 0;
    preg_match_all('/(\d+)/', $str, $matches);
    if(isset($matches[0])) {
        $ret = intval(implode('', $matches[0]));
    }
    return $ret;
}

function getProxy() {
    global $proxies;
    if(!count($proxies)) {
        return '0.0.0.0';
    } else {
        $randKey = array_rand($proxies);
        $rand = $proxies[$randKey];
        return $rand['proxy_ip'] . ':' . $rand['proxy_port'];
    }
}

function transformArrayKeys($arr) {
    $ret = array();
    foreach($arr as $el) {
        $newKey = array_shift($el);
        $ret[$newKey] = $el;
    }
    return $ret;
}

function clearDir($dir) {
    if ($objs = glob($dir."/*")) {
        foreach($objs as $obj) {
            is_dir($obj) ? clearDir($obj) : unlink($obj);
        }
    }
}

function setLog($action, $userUid, $text, $themeId) {
    global $db;
    $inserts = array(
        'log_action' => $action,
        'log_userUid' => $userUid,
        'log_text' => $text,
        'log_themeId' => $themeId
    );
    $sql = "INSERT INTO `log` SET ?set";
    $logId = $db->query($sql, array($inserts), 'id');
    return $logId;
}

function getSettings() {
    global $db;
    $sql = "SELECT * FROM `settings`";
    $result = $db->query($sql)->assoc();
    return transformArrayKeys($result);
}

function updateSettings($settings) {
    global $db;
    foreach($settings as $key => $set) {
        if(substr($key, 0, 8) == 'setting_' && is_numeric(substr($key, 8))) {
            $id = substr($key, 8);
            $sql = "UPDATE `settings` SET `setting_value`=?i WHERE `setting_id`=?i";
            $db->query($sql, array($set, $id), 'ar');
        }
    }
    return true;
}

function getUsers() {
    global $db;
    $sql = "SELECT * FROM `users`";
    $result = $db->query($sql)->assoc();
    return $result;
}

function saveUsers($usersStr) {
    global $db;
    $users = explode("\n", str_replace("\r\n", "\n", $usersStr));
    $db->query("TRUNCATE `users`");
    foreach($users as $user) {
        $userParse = explode(':', $user);
        if($userParse[0] && $userParse[1]) {
            $sql = "INSERT INTO `users`
                    SET `user_login`=?,
                        `user_password`=?";
            $db->query($sql, array($userParse[0], $userParse[1]), 'id');
        }
    }
    return true;
}

function getProxies() {
    global $db;
    $sql = "SELECT * FROM `proxies`";
    $result = $db->query($sql)->assoc();
    return $result;
}

function saveProxies($proxiesStr) {
    global $db;
    $proxies = explode("\n", str_replace("\r\n", "\n", $proxiesStr));
    $db->query("TRUNCATE `proxies`");
    foreach($proxies as $proxy) {
        $proxyParse = explode(':', $proxy);
        if($proxyParse[0] && $proxyParse[1]) {
            $sql = "INSERT INTO `proxies`
                    SET `proxy_ip`=?,
                        `proxy_port`=?";
            $db->query($sql, array($proxyParse[0], $proxyParse[1]), 'id');
        }
    }
    return true;
}

function getTopics() {
    global $db;
    $sql = "SELECT * FROM `topics`";
    $result = $db->query($sql)->assoc();
    return $result;
}

function getBadTopics() {
    global $db;
    $sql = "SELECT * FROM `badTopics`";
    $result = $db->query($sql)->assoc();
    return $result;
}

function saveTopics($topicsStr) {
    global $db;
    $topics = explode("\n", str_replace("\r\n", "\n", $topicsStr));
    $db->query("TRUNCATE `topics`");
    foreach($topics as $topic) {
        if($topic) {
            $topicTitle = checkTopic($topic, true);
            if($topicTitle !== false) {
                $sql = "INSERT INTO `topics` SET `topic_url`=?, `topic_title`=?";
                $db->query($sql, array($topic, $topicTitle), 'id');
            } else {
                $sql = "INSERT INTO `badTopics` SET `badTopic_url`=?, `badTopic_title`=?";
                $db->query($sql, array($topic, ''), 'id');
            }
        }
    }
    return true;
}

function getPhrases() {
    global $db;
    $sql = "SELECT * FROM `phrases`";
    $result = $db->query($sql)->vars();
    return $result;
}

function savePhrases($phrasesStr) {
    global $db;
    $phrases = explode("\n", str_replace("\r\n", "\n", $phrasesStr));
    $db->query("TRUNCATE `phrases`");
    foreach($phrases as $phrase) {
        if($phrase) {
            $sql = "INSERT INTO `phrases`
                    SET `phrase_txt`=?";
            $db->query($sql, array($phrase), 'id');
        }
    }
    return true;
}
