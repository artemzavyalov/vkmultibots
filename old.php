<?php
$user_agent = 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.79 Safari/537.4';

$cookie = dirname(__FILE__) . '/cookie.txt';

$login = 'artemzavyalov@yandex.ru';

$password = '1989artzav';

// Функция, которая позволяет нам переходить по редиректам с включенной опцией open_basedir
function curl_redirect_exec($ch, $redirects = 0, $curlopt_returntransfer = true, $curlopt_maxredirs = 10, $curlopt_header = false) {
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $data = curl_exec($ch);
    $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $exceeded_max_redirects = $curlopt_maxredirs > $redirects;
    $exist_more_redirects = false;
    if ($http_code == 301 || $http_code == 302) {
        if ($exceeded_max_redirects) {
            list($header) = explode("\r\n\r\n", $data, 2);
            $matches = array();
            preg_match('/(Location:|URI:)(.*?)\n/', $header, $matches);
            $url = trim(array_pop($matches));
            $url_parsed = parse_url($url);
            if (isset($url_parsed)) {
                curl_setopt($ch, CURLOPT_URL, $url);
                $redirects++;
                return curl_redirect_exec($ch, $redirects, $curlopt_returntransfer, $curlopt_maxredirs, $curlopt_header);
            }
        }
        else {
            $exist_more_redirects = true;
        }
    }
    if ($data !== false) {
        if (!$curlopt_header)
        list(,$data) = explode("\r\n\r\n", $data, 2);
        if ($exist_more_redirects) return false;
        if ($curlopt_returntransfer) {
            return $data;
        }
        else {
            echo $data;
            if (curl_errno($ch) === 0) return true;
            else return false;
        }
    }
    else {
        return false;
    }
}

$ch = curl_init();

// чтобы сайт думал, что мы - браузер:
curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);

// ответ сервера будем записывать в переменную
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

curl_setopt($ch, CURLOPT_TIMEOUT, 300);

//curl_setopt($ch, CURLOPT_HEADER, 1);

curl_setopt($ch, CURLOPT_REFERER, 'http://m.vk.com/login?fast=1&hash=&s=0&to=');

curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);

curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_URL, 'https://login.vk.com/?act=login&from_host=m.vk.com&from_protocol=http&ip_h=&pda=1');

//curl_setopt($ch, CURLOPT_INTERFACE, '194.28.85.193');
//curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);
//curl_setopt($ch, CURLOPT_PROXY, "178.33.181.120:8080");
//curl_setopt($ch, CURLOPT_PROXYUSERPWD, "");

$answer = curl_redirect_exec($ch);
//echo $answer;

//формируем запрос
$post = array(
'email' => $login,
'pass' => $password
);

curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));

// собственно этот ответ сервера уже доказывает что мы авторизировались
if(!file_exists($cookie)) {
$answer = curl_redirect_exec($ch);
//echo $answer;
}

curl_setopt($ch, CURLOPT_POST, false);

// укажем страницу, с которой мы получим данные для проверки. она может быть как с мобильного так и с основного сайта
curl_setopt($ch, CURLOPT_URL, 'http://m.vk.com/');

echo curl_redirect_exec($ch);

//curl_setopt($ch, CURLOPT_URL, 'http://m.vk.com/friends');

//echo curl_redirect_exec($ch);
//$answer = str_get_html(curl_redirect_exec($ch));

//foreach($answer->find('ul.main_menu li a') as $e){
//    echo $e->href . '<br>';
//}

curl_close($ch);
