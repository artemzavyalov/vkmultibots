<?php

require_once 'inc.php';

if(count($_POST) > 0) {
    updateSettings($_POST);
    if(isset($_POST['users']) && $_POST['users']) {
        saveUsers($_POST['users']);
    }
    if(isset($_POST['proxies']) && $_POST['proxies']) {
        saveProxies($_POST['proxies']);
    }
    if(isset($_POST['topics']) && $_POST['topics']) {
        saveTopics($_POST['topics']);
    }
    if(isset($_POST['phrases']) && $_POST['phrases']) {
        savePhrases($_POST['phrases']);
    }
    if(isset($_POST['saveTopic']) && $_POST['saveTopic']) {
        $sql = "DELETE FROM `badTopics` WHERE `badTopic_id`=?i";
        $db->query($sql, array($_POST['saveTopic']), 'ar');
        $sql = "INSERT INTO `topics` SET `topic_url`=?, `topic_title`=?";
        $db->query($sql, array($_POST['saveTxt'], ''), 'id');
        die;
    }
}

$settings = getSettings();
$proxies = getProxies();
$users = getUsers();
$topics = getTopics();
$badTopics = getBadTopics();
$phrases = getPhrases();

$usersStr = '';
foreach($users as $user) {
    $usersStr .= "{$user['user_login']}:{$user['user_password']}\n";
}

$proxiesStr = '';
foreach($proxies as $proxy) {
    $proxiesStr .= "{$proxy['proxy_ip']}:{$proxy['proxy_port']}\n";
}

$topicsStr = '';
foreach($topics as $topic) {
    $topicsStr .= "{$topic['topic_url']}\n";
}

$phrasesStr = '';
foreach($phrases as $phrase) {
    $phrasesStr .= "{$phrase}\n";
}

require 'pages/settings.php';
