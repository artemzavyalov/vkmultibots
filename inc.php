<?php

define('ROOT_DIR', str_replace('\\', '/', dirname(__FILE__)));
require_once ROOT_DIR . '/libs/goDB/autoload.php';
require_once ROOT_DIR . '/libs/goDB/Result.php';
require_once ROOT_DIR . '/libs/goDB/DB.php';
require_once ROOT_DIR . '/libs/goDB/Adapters/mysql.php';
require_once ROOT_DIR . '/libs/goDB/Helpers/Config.php';
require_once ROOT_DIR . '/libs/goDB/Helpers/Connector.php';
require_once ROOT_DIR . '/libs/goDB/Helpers/Fetcher.php';
require_once ROOT_DIR . '/libs/goDB/Helpers/Templater.php';
require_once ROOT_DIR . '/libs/goDB/Helpers/ParserPH.php';
require_once ROOT_DIR . '/libs/goDB/Implementations/Base.php';
require_once ROOT_DIR . '/libs/goDB/Implementations/mysql.php';
require_once ROOT_DIR . '/libs/goDB/Exceptions/Exception.php';
require_once ROOT_DIR . '/libs/goDB/Exceptions/Logic.php';
require_once ROOT_DIR . '/libs/goDB/Exceptions/Query.php';

require_once ROOT_DIR . '/libs/simple_html_dom.php';

require_once ROOT_DIR . '/functions.php';


// Configs
$phpPath = '/usr/bin/php';
$proxy = ROOT_DIR . '/proxy.txt';
$user = ROOT_DIR . '/user.txt';
$dbConfig['host']     = '78.47.11.61';
$dbConfig['username'] = 'root';
$dbConfig['password'] = 'HNymKU49';
$dbConfig['dbname']   = 'vk-script';
$dbConfig['charset']  = 'utf8';

$db = go\DB\DB::create($dbConfig, 'mysql', true);

$settings = getSettings();
$proxies = getProxies();
$users = getUsers();

$ipProxy = '';
