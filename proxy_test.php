<?php

$user_agent = 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.79 Safari/537.4';

$proxy = dirname(__FILE__) . '/proxy.txt';
$cookie = dirname(__FILE__) . '/cookie.txt';
$testUrl = 'http://login.vk.com/';

$proxies = explode("\n", str_replace("\n\r", "\n", file_get_contents($proxy)));
$strings = '';
foreach($proxies as $ipProxy) {
    $answer = send($ipProxy);
    if(strpos($answer, '200 OK')) {
        $strings .= "$ipProxy\n";
    }
}
$strings = substr($strings, 0, -1);
file_put_contents($proxy, $strings);

function send($ipProxy) {
    global $user_agent, $testUrl;
    $ch = curl_init($testUrl);
    curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_REFERER, '');
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXY, $ipProxy);
    $result = curl_exec($ch);
    return $result;
}

